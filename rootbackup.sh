#!/bin/bash
##################################
# This script backups the root-dir to a certain path as tar.gz-archive and can copy old backups to a certain location.
# 12/11/13, 12:27 PM
#################################

# init logger
$logfile=/var/log/backup.log
exec > $logfile 2>&1

# ****************
# CHECK CALLER
# ****************
# check for root; id = 0 equals root
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root!" 1>&2 # 1>&2 = stderr > stdout
	exit 1
fi

# check caller
lppid=`expr $PPID + 1`
# show process-tree, filter by this processes PID and get the caller w/ awk
CALLER=$(ps xf | grep "^ *$lppid" | awk {'print $6'}) 
echo $lppid
echo $CALLER
if [ "$CALLER" != "bash" ]; then
	echo "This script must be run with bash and not sh!" 1>&2 
	exit 1
fi

# ****************
# FUNCTIONS
# ****************
# Usage
usage()
{
cat << EOF
usage: $0 options

This script backups the root-dir to a certain path as tar.gz-archive and can copy old backups to a certain location.

OPTIONS:
   -h      Show this message
   -p      Backup path. 
   -s      Preserve space by copying to 2nd HDD; Expects a path
   -t      Integer. When preserving space, how many days of backups should be moved?
   -v      Verbose
   
EXAMPLE:
$ rootbackup -v -bp "/mnt/1TBBackup/debian" -ps "/mnt/2TB/debian_backups_OLD" -t 5
This will backup / to "/mnt/1TBBackup/debian" and copy backups older than 5 days to "/mnt/2TB/debian_backups_OLD/"

EOF
}

# moveold(path, time)
moveold()
{
# Remove old files
echo "-------------------" 
echo "$(date): Moving old backups..." 
find $TARGET -mtime +$TIME -exec mv -v {} $2NDTARGET/ \; 
}
# find /mnt/1TBBackup/debian/ -mtime +5 -exec mv -v {} /mnt/2TB/debian_backups_OLD/ \; >> /var/log/backup.log

backup()
{
echo "$(date): Running backup..." 
tar -zcvpf $TARGET/$(date --iso)-fullbackup.tar.gz --directory=/ --exclude=proc --exclude=sys --exclude=dev/pts --exclude=backups --exclude=mnt .

echo "$(date): Finished!" 
}

# ****************
# INIT
# ****************
# get parameter w/ getopts
TARGET=
2NDTARGET=
TIME=
VERBOSE=

while getopts “hp:stv” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         p)
             TARGET={$OPTARG%/}
             ;;
         s)
             2NDTARGET={$OPTARG%/}
             ;;
         t)
             TIME=$OPTARG
             ;;
         v)
             VERBOSE=1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

# if path isn't provided, exit
if [[ -z $TARGET ]]
then
     usage
     exit 1
fi

# else, run backups of backup, lol
if [[ -z $2NDTARGET ]] && [[ -z $TIME ]] && [[ $TIME = *[0-9]* ]]
then
	# shell parameter extension, remove trailing /
	usage
	exit 1
else
     moveold
fi

# then, run backup
backup

# end

